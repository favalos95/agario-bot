#include "bot.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double myx;
double myy;



int dist_food(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

int dist_cell(const void *a, const void *b)
{
    double ax = (*(struct cell *)a).x;
    double ay = (*(struct cell *)a).y;
    double bx = (*(struct cell *)b).x;
    double by = (*(struct cell *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

int dist_player(const void *a, const void *b)
{
    double ax = (*(struct player *)a).x;
    double ay = (*(struct player *)a).y;
    double bx = (*(struct player *)b).x;
    double by = (*(struct player *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    // Copy Player's x-y to global variables
    myx = me.x;
    myy = me.y;
    
    // Sort the food in an array with 0 being the closest
    qsort(foods, nfood, sizeof(struct food), dist_food);
    
    // Sort the virus cells with 0 being the cloeset
    qsort(virii, nvirii, sizeof(struct cell), dist_cell);
    
    // Sort the player cell mass with 0 being the closest
    qsort(mass, nmass, sizeof(struct cell), dist_cell);
    
    // Sort the players nearby with 0 being the closest
    qsort(players, nplayers, sizeof(struct player), dist_player);
    
    
    /**
    * This is the start of the actual bot dynamics
    * 
    * The above was for setup of each variable,
    * at least in this version of agar.io
    * 
    * The below must have no errors, 
    * as well as being able to keep working throughout its play-time
    * 
    */
    
    
    

    
    // Move towards the closest mass cell
    if(mass[0].x > 0.001 && mass[0].y > 0.001)
    {
        act.dx = mass[0].x - me.x;
        act.dy = mass[0].y - me.y;
    }
    

    /**
     * 
     * Check for players constantly 
     * 
     */
    if(players[0].x > 0.001 && players[0].y != 0.001 && me.totalMass < 130.0 && (players[0].totalMass*.76) > me.totalMass && virii[0].x > 0.001)
    {
        act.dx = virii[0].x - me.x;
        act.dy = virii[0].y - me.y;
    }
    if(players[0].x > 0.001 && players[0].y > 0.001 && players[0].totalMass < (me.totalMass*.76))
    {
        act.dx = players[0].x - me.x;
        act.dy = players[0].y - me.y;
    }
    if(players[0].x > 0.001 && players[0].y > 0.001 && (players[0].totalMass*.76) > me.totalMass)
    {
        act.dx = me.x - players[0].x;
        act.dy = me.y - players[0].y;
    }   
    

    

    


    // Move towards the closest food from the array
    if(players[0].x < 0.01 && players[0].y < 0.01 && foods[0].x != 0.0 && foods[0].y != 0.0)
    {
    act.dx = foods[0].x - me.x;
    act.dy = foods[0].y - me.y;
    }
    if(foods[0].x < 0.001 && foods[0].y < 0.001 && me.x > 2500 && me.y > 2500)
    {
        act.dx = -600;
        act.dy = -600;

    }
    if(foods[0].x < 0.001 && foods[0].y < 0.001 && me.x <= 2500 && me.y > 2500)
    {
        act.dx = 600;
        act.dy = -60;

    }
    if(foods[0].x < 0.001 && foods[0].y < 0.001 && me.x > 2500 && me.y <= 2500)
    {
        act.dx = -60;
        act.dy = 600;

    }
    if(foods[0].x < 0.01 && foods[0].y < 0.001 && me.x <= 2500 && me.y <= 2500)
    {
        act.dx = 600;
        act.dy = 600;

    }
    

    
    
    
    if(me.ncells > 1)
    {
    act.dx = 2;
    act.dy = 2;
    }
    act.split = 0;
    act.fire = 0;
    
    //printf("%f\n", players[0].x);
    return act;
}